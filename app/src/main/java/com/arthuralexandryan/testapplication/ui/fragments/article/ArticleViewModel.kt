package com.arthuralexandryan.testapplication.ui.fragments.article

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import com.arthuralexandryan.testapplication.di.EntryArticleRepo
import com.arthuralexandryan.testapplication.di.EntryArticleRepoImpl

class ArticleViewModel(application: Application) : AndroidViewModel(application){
    private var repository: EntryArticleRepoImpl = EntryArticleRepoImpl()

    fun getAllArticles() = repository.getAllArticles()
}