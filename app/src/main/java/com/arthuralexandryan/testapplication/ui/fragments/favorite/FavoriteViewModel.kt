package com.arthuralexandryan.testapplication.ui.fragments.favorite

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import com.arthuralexandryan.testapplication.di.EntryArticleRepo
import com.arthuralexandryan.testapplication.di.EntryArticleRepoImpl

class FavoriteViewModel(application: Application) : AndroidViewModel(application){
    private var repository: EntryArticleRepoImpl = EntryArticleRepoImpl()

    fun getFavorites() = repository.getFavoriteArticles()
}