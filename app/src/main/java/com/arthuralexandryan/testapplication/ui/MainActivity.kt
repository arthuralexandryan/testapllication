package com.arthuralexandryan.testapplication.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.arthuralexandryan.testapplication.R
import com.arthuralexandryan.testapplication.di.article.ArticleRequestPresenter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val artPre: ArticleRequestPresenter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getArticleList()

        val fragmentAdapter = ArticlePagerAdapter(supportFragmentManager)
        viewpager_main.adapter = fragmentAdapter

        tabs_main.setupWithViewPager(viewpager_main)
    }

    private fun getArticleList(){
        runBlocking {
            coroutinSum()
        }
    }

    private suspend fun coroutinSum() = coroutineScope {
        launch {
            artPre.getRequest()
        }
    }
}
