package com.arthuralexandryan.testapplication.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.arthuralexandryan.testapplication.ui.fragments.article.ArticleFragment
import com.arthuralexandryan.testapplication.ui.fragments.favorite.FavoriteFragment

class ArticlePagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                ArticleFragment()
            }
            else -> {
                return FavoriteFragment()
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Articles"
            else -> {
                return "Favorites"
            }
        }
    }
}