package com.arthuralexandryan.testapplication.ui.fragments.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.arthuralexandryan.testapplication.R
import com.arthuralexandryan.testapplication.db.entries.ArticleModelEntry
import com.arthuralexandryan.testapplication.ui.fragments.ArticleAdapter
import kotlinx.android.synthetic.main.fragment_article.*

class FavoriteFragment : Fragment() {

    private var favoriteViewModel: FavoriteViewModel? = null

    lateinit var adapter: ArticleAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_article, container, false).apply {
            favoriteViewModel = ViewModelProviders
                .of(this@FavoriteFragment)
                .get(FavoriteViewModel::class.java)

            favoriteViewModel?.getFavorites()?.observe(viewLifecycleOwner,
                Observer<List<ArticleModelEntry>> { this@FavoriteFragment.renderArticles(it) })
        }
    }

    private fun renderArticles(articles: List<ArticleModelEntry>){
        adapter =
            ArticleAdapter(
                this.context,
                articles
            )
        val layoutManager = LinearLayoutManager(this.context)
        layoutManager.stackFromEnd = true
        article_list.layoutManager = layoutManager
        article_list.adapter = adapter
    }
}