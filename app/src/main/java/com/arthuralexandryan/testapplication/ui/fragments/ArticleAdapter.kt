package com.arthuralexandryan.testapplication.ui.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.CheckBox
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.arthuralexandryan.testapplication.AppClass
import com.arthuralexandryan.testapplication.R
import com.arthuralexandryan.testapplication.db.entries.ArticleModelEntry
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class ArticleAdapter (val context: Context?, val articles: List<ArticleModelEntry>): RecyclerView.Adapter<ArticleAdapter.ViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_article, parent, false))
    }

    override fun getItemCount(): Int = articles.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.animation = AnimationUtils.loadAnimation(context, R.anim.item_animation_fall_down)
        holder.title.text = articles[position].title
        holder.webUrl.text = articles[position].sectionName
        holder.favorite.isChecked = articles[position].favorite

        holder.favorite.setOnCheckedChangeListener { buttonView, isChecked ->
            insertToDB(ArticleModelEntry(articles[position].title, articles[position].sectionName, isChecked))
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var title: AppCompatTextView = itemView.findViewById(R.id.txt_Title) as AppCompatTextView
        var webUrl: AppCompatTextView = itemView.findViewById(R.id.txt_url) as AppCompatTextView
        var favorite: CheckBox = itemView.findViewById(R.id.chk_favorites) as CheckBox
    }

    private fun insertToDB(article: ArticleModelEntry) = runBlocking {
        withContext(Dispatchers.Default) {
            AppClass.database.articleDao().insertArticle(article)
        }
    }
}