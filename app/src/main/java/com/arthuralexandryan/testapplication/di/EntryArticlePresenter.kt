package com.arthuralexandryan.testapplication.di

class EntryArticlePresenter (private val articleRepo: EntryArticleRepo){
    fun listArticles() = articleRepo.getAllArticles()
    fun listFavoritesArticles() = articleRepo.getFavoriteArticles()
}