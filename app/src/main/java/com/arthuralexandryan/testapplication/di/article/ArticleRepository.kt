package com.arthuralexandryan.testapplication.di.article

import com.arthuralexandryan.testapplication.api.ApiClient
import com.arthuralexandryan.testapplication.api.responseModel.ResponseModel
import retrofit2.Call

interface ArticleRepository {
    fun getArticles(): Call<ResponseModel>
}

class ArticleRepositoryImpl : ArticleRepository {
    override fun getArticles(): Call<ResponseModel> = ApiClient().createRequest().getArticles()
}