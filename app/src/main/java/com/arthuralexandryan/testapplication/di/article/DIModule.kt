package com.arthuralexandryan.testapplication.di.article

import org.koin.dsl.module

val articleModule = module {
    single<ArticleRepository>{ ArticleRepositoryImpl() }
    factory {
        ArticleRequestPresenter(
            get()
        )
    }
}