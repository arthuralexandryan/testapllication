package com.arthuralexandryan.testapplication.di

import org.koin.dsl.module


var dbModule = module {
    single<EntryArticleRepo> { EntryArticleRepoImpl() }
    factory { EntryArticlePresenter(get()) }
}