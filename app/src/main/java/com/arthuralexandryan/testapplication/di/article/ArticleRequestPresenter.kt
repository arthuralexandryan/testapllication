package com.arthuralexandryan.testapplication.di.article

import com.arthuralexandryan.testapplication.AppClass
import com.arthuralexandryan.testapplication.api.responseModel.ResponseModel
import com.arthuralexandryan.testapplication.db.entries.ArticleModelEntry
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ArticleRequestPresenter(private val artReq: ArticleRepository): Callback<ResponseModel> {

    fun getRequest(){
        artReq.getArticles().enqueue(this)
    }

    override fun onFailure(call: Call<ResponseModel>, t: Throwable) {
        t.printStackTrace()
    }

    override fun onResponse(call: Call<ResponseModel>, response: Response<ResponseModel>) {
        try {
            if (response.isSuccessful && response.body() != null){
                response.body()?.let {
                    val list: ArrayList<ArticleModelEntry> = ArrayList()
                    it.response?.results?.map {result ->
                        list.add(ArticleModelEntry(title = result.webTitle, sectionName = result.sectionName))
                    }
                    insertToDB(list)
                }
            }
        } catch (e: Exception){
            e.printStackTrace()
        }
    }

    private fun insertToDB(list: ArrayList<ArticleModelEntry>) = runBlocking {
        withContext(Dispatchers.Default) {
            AppClass.database.articleDao().insert(list)
        }
    }
}