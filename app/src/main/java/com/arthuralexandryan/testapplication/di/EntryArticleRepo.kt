package com.arthuralexandryan.testapplication.di

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.arthuralexandryan.testapplication.AppClass
import com.arthuralexandryan.testapplication.db.dao.ArticleDao
import com.arthuralexandryan.testapplication.db.entries.ArticleModelEntry
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

interface EntryArticleRepo {
    fun getAllArticles(): LiveData<List<ArticleModelEntry>>
    fun getFavoriteArticles(): LiveData<List<ArticleModelEntry>>
}

class EntryArticleRepoImpl : EntryArticleRepo, CoroutineScope{

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    private var articleDao: ArticleDao = AppClass.database.articleDao()

    override fun getAllArticles(): LiveData<List<ArticleModelEntry>> = articleDao.getAllArticles()

    override fun getFavoriteArticles(): LiveData<List<ArticleModelEntry>> = articleDao.getFavoriteArticles()

}