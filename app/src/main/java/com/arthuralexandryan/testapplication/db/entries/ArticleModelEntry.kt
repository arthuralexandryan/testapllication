package com.arthuralexandryan.testapplication.db.entries

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "article_table")
data class ArticleModelEntry (
    @PrimaryKey
    var title: String,
    var sectionName: String?,
    var favorite: Boolean = false
)