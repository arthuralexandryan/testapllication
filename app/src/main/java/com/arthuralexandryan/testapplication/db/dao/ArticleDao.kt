package com.arthuralexandryan.testapplication.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.arthuralexandryan.testapplication.db.entries.ArticleModelEntry

@Dao
interface ArticleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(list: List<ArticleModelEntry>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertArticle(article: ArticleModelEntry)

    @Query("SELECT * FROM article_table")
    fun getAllArticles(): LiveData<List<ArticleModelEntry>>

    @Query("SELECT * FROM article_table WHERE favorite = 1")
    fun getFavoriteArticles(): LiveData<List<ArticleModelEntry>>
}