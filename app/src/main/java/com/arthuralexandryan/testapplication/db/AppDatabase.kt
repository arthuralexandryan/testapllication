package com.arthuralexandryan.testapplication.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.arthuralexandryan.testapplication.db.dao.ArticleDao
import com.arthuralexandryan.testapplication.db.entries.ArticleModelEntry

@Database(entities = [(
        ArticleModelEntry::class
        )], version = 1)
abstract class AppDatabase : RoomDatabase(){
    abstract fun articleDao(): ArticleDao
}