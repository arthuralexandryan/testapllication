package com.arthuralexandryan.testapplication

import android.app.Application
import com.arthuralexandryan.testapplication.di.article.articleModule
import androidx.room.Room
import com.arthuralexandryan.testapplication.db.AppDatabase
import com.arthuralexandryan.testapplication.di.dbModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class AppClass : Application() {
    companion object {
        lateinit var database: AppDatabase
    }

    override fun onCreate() {
        super.onCreate()

        database = Room.databaseBuilder(this, AppDatabase::class.java, "app_database").build()

        startKoin {
            androidLogger()
            androidContext(this@AppClass)
            modules(articleModule)
            modules(dbModule)
        }
    }
}