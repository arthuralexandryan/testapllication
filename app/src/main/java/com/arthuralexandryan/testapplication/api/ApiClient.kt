package com.arthuralexandryan.testapplication.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

class ApiClient : BaseApiClient() {
    override val builder: Retrofit = Retrofit.Builder()
        .baseUrl("https://content.guardianapis.com/")
        .client(getClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    override fun createRequest(): ApiInterface = builder.create(ApiInterface::class.java)

}