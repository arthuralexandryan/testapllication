package com.arthuralexandryan.testapplication.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit

abstract class BaseApiClient {
    abstract val builder: Retrofit
    abstract fun createRequest(): ApiInterface

    val getClient = OkHttpClient.Builder()
        .build()
}