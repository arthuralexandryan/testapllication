package com.arthuralexandryan.testapplication.api.responseModel

data class Edition (
    var id: String?,
    var webTitle: String?,
    var webUrl: String?,
    var apiUrl: String?,
    var code: String?
)