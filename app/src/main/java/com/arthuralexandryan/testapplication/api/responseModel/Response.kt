package com.arthuralexandryan.testapplication.api.responseModel

data class Response(
    var status: String?,
    var userTier: String?,
    var total: Int?,
    var startIndex: Int?,
    var pageSize: Int?,
    var currentPage: Int?,
    var pages: Int?,
    var orderBy: String?,
    var results: List<Result>?
)