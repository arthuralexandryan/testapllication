package com.arthuralexandryan.testapplication.api

import com.arthuralexandryan.testapplication.api.responseModel.ResponseModel
import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {

    @GET("tags?q=apple&section=technology&show-references=all&api-key=test")
    fun getArticles(): Call<ResponseModel>
}