package com.arthuralexandryan.testapplication.api.responseModel

data class Result(
    var id: String?,
    var sectionId: String?,
    var sectionName: String?,
    var webPublicationDate: String?,
    var webTitle: String,
    var webUrl: String?,
    var apiUrl: String?
)